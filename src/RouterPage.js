import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import App from "./App";
import Register from "./components/Register";

class RouterPage extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path={'/'} component={Register}/>
                    <Route exact path={'/cabinet/user'} component={App}/>
                </Switch>
            </div>
        );
    }
}
export default RouterPage;
