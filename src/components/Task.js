import React from "react";
import {useMutation} from "@apollo/client";
import {Table} from 'reactstrap';

import {GET_TODOS, REMOVE_TODO, TOGGLE_COMPLETED} from "../graphql/Queries";
import "../styles/Task.css";

const Task = ({todo}) => {

    const getUserAccount = () => {
        const jsonString = localStorage.getItem('account');
        const userAccount = jsonString ? JSON.parse(jsonString) : null;
        return userAccount ? userAccount.role : '';
    }

    const [removeTodoMutation] = useMutation(REMOVE_TODO);
    const [toggleCompletedMutation] = useMutation(TOGGLE_COMPLETED);

    const removeTodo = (id) => {
        removeTodoMutation({
            variables: {id},
            optimisticResponse: true,
            update: (cache) => {
                const existingTodos = cache.readQuery({query: GET_TODOS});
                const todos = existingTodos.todos.filter((t) => t.id !== id);
                cache.writeQuery({
                    query: GET_TODOS,
                    data: {todos},
                });
            },
        });
    };

    const toggleCompleted = ({id, completed}) => {
        toggleCompletedMutation({
            variables: {id, completed: !completed},
            optimisticResponse: true,
            update: (cache) => {
                const existingTodos = cache.readQuery({query: GET_TODOS});
                const updatedTodo = existingTodos.todos.map((todo) => {
                    if (todo.id === id) {
                        return {...todo, completed: !completed};
                    } else {
                        return todo;
                    }
                });
                cache.writeQuery({
                    query: GET_TODOS,
                    data: {todos: updatedTodo},
                });
            },
        });
    };

    return (
        <div key={todo.id}>
            <div className="row">
                <div className="col-md-4 offset-4">
                    <Table hover className="text-center">
                        {/*<thead>*/}
                        {/*<tr>*/}
                        {/*    <th>Done</th>*/}
                        {/*    <th>Description</th>*/}
                        {/*    <th>Opt</th>*/}
                        {/*</tr>*/}
                        {/*</thead>*/}
                        <tbody>
                        <tr>
                            <td>
                                <input
                                    type="checkbox"
                                    checked={todo.completed}
                                    onChange={() => toggleCompleted(todo)}
                                />
                            </td>
                            <td><span className={todo.completed ? "completed" : ""}>{todo.task}</span></td>
                            <td>
                                <button type="submit" onClick={() => removeTodo(todo.id)}
                                        className="bg-danger removeButton">
                                    x
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        </div>
    );
};

export default Task;


