import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { ADD_TODO, GET_TODOS } from "../graphql/Queries";
import '../App.css'
import {useHistory} from "react-router-dom";
const updateCache = (cache, { data }) => {
    const existingTodos = cache.readQuery({
        query: GET_TODOS,
    });

    const newTodo = data.insert_todos_one;
    cache.writeQuery({
        query: GET_TODOS,
        data: { todos: [...existingTodos.todos, newTodo] },
    });
};

export default  () => {
    let history = useHistory();
    const [task, setTask] = useState("");
    const [addTodo] = useMutation(ADD_TODO, { update: updateCache });

    const submitTask = () => {
        addTodo({ variables: { task } });
        setTask("");
    };

   const logout=()=> {
        console.log(localStorage.getItem('account'))
        localStorage.removeItem('account')
        history.push('/')
    }

    return (
        <div>
            <input
                className="taskInput"
                type="text"
                placeholder="Add a new task"
                value={task}
                onChange={(e) => setTask(e.target.value)}
                onKeyPress={(e) => {
                    if (e.key === "Enter") submitTask();
                }}
            />
            <button onClick={submitTask}>Add</button>
            <button onClick={() =>logout()}>Log out</button>
        </div>
    );
};
