import React from "react";
import { useQuery } from "@apollo/client";

import { GET_TODOS } from "../graphql/Queries";
import Task from "./Task";

const Tasks = () => {
    const { loading, error, data } = useQuery(GET_TODOS);

    if (loading) {
        return <div className="tasks mt-5 text-center">Loading...</div>;
    }
    if (error) {
        return <div className="tasks mt-5 text-center">Error!</div>;
    }

    return (
        <div className="tasks">
            {data.todos.map((todo) => (
                <Task key={todo.id} todo={todo} />
            ))}
        </div>
    );
};

export default Tasks;
