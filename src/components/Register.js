import React, {useEffect} from 'react';
import {useForm} from 'react-hook-form';
import {useHistory} from "react-router-dom";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import '../App.css'

const getUserAccount = () => {
    const jsonString = localStorage.getItem('account');
    const userAccount = jsonString ? JSON.parse(jsonString) : null;
    return userAccount ? userAccount.role : '';
}

const saveUserAccount = (user) => {
    localStorage.removeItem('account')
    localStorage.setItem('account', JSON.stringify(user));

}
const notifySuccess = () => toast.error("Please choose a role !!!");

export default function Register() {
    useEffect(() => {
        const role = getUserAccount();
        console.log(role)
        if (role === 'admin') {
            history.push('/cabinet/user')
        } else if (role === 'user') {
            history.push('/cabinet/user')
        }
    }, [])
    const {user} = ''
    let history = useHistory();
    const {register, handleSubmit} = useForm();
    const onSubmit = (data) => {
        saveUserAccount(data)
        if (data.role === 'admin') {
            history.push('/cabinet/user')
            console.log("USER:  " + localStorage.getItem(user))
        } else if (data.role === 'user') {
            history.push('/cabinet/user')
        } else {
            notifySuccess();
        }
    }
    return (
        <div className="container">
            <ToastContainer/>
            <div className="row my-5 regForm">
                <div className="col-md-1">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <input name="firstName" ref={register} placeholder="Name" className="my-2"/>
                        <select name="role" ref={register} className="selectForm">
                            <option value="" disabled={true} selected={true}>Select role</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>
                        <input type="submit" className="sendForm mt-2"/>
                    </form>
                </div>
            </div>
        </div>
    );
}
